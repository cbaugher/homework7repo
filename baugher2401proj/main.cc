///////////////////////////////////////////////
//            Catherine Baugher              //
//            Project 7                      //
//            23 November 2015               //
///////////////////////////////////////////////
#include <iostream>
#include "othello.h"
#include "game.h"
using namespace std;
int main(){
	Othello othgame;
	bool playing = true;
	char select;
	string move;
	while(playing){
		othgame.play();
		//cout << "\nWinner is";
		//if(othgame.play() == 2){
		//	cout << "not you." << endl;
	//	}else{
	//		cout << "you!" << endl;
	//	}
		cout << "\nWould you like to go again? <y, n>: ";
		cin >> select;
		if(select == 'n' || select == 'N'){
			playing = false;
		}
	}
	cout << "Thanks for playing!" << endl;
	return 0;
}
