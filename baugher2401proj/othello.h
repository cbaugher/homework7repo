///////////////////////////////////////////////
//            Catherine Baugher              //
//            Project 7                      //
//            23 November 2015               //
///////////////////////////////////////////////
#ifndef OTHELLO_H
#define OTHELLO_H
#include "game.h"
//piece stuff
class piece{
	public:
	piece(){black = 0; white = 0; empty = 1;}
	void flip();

	bool is_black()const{return black;}
	bool is_white()const{return white;}
	bool is_empty()const{return empty;}
	
	void set_black(){black = 1; white = 0; empty = 0;}
	void set_white(){black = 0; white = 1; empty = 0;}
	void set_empty(){black = 0; white = 0; empty = 1;}
	private:
	bool black;
	bool white;
	bool empty;
};
//board stuff
using namespace main_savitch_14;
int get_column(char move);
class Othello : public game{
	public:
	Othello(){board[8][8]; movenum = 0; skips = 0;}
	bool is_game_over( ) const;
	bool is_legal(const std::string & move) const;
	void restart();
	void make_move(const std::string & move);

	int compute_moves(std::queue<std::string>& moves) const;
	bool check_moves();
	game* clone( ) const{return new Othello(*this);}
	int evaluate( ) const;
	who last_mover( ) const
	    {return (movenum % 2 == 1 ? HUMAN : COMPUTER); }
	who next_mover( ) const
	    { return (movenum % 2 == 1 ? HUMAN : COMPUTER); }
	int moves_completed( ) const{return movenum;}	

	void display_status()const;
	void display_message(const std::string& message) const;

	int countwhite() const;
	int countblack() const;
	private:
	piece board[8][8];
	std::queue <std::string> dummy;
	int movenum;
	int skips;
};
#endif
