///////////////////////////////////////////////
//            Catherine Baugher              //
//            Project 7                      //
//            23 November 2015               //
///////////////////////////////////////////////
#include <iostream>
#include "othello.h"
#include "colors.h"
using namespace std;
//piece stuff
void piece::flip() {
	if (white) {
		black = 1;
		white = 0;
	}
	else if (black) {
		black = 0;
		white = 1;
	}
}
//board stuff
void Othello::display_status()const {
	string tmpmove;
	cout << B_BLACK << RED << "    A    B    C    D    E    F    G    H   " << endl;
	for (int i = 0; i < 8; i++) {
		cout << B_BLACK << BLUE << " |----------------------------------------|";
		cout << endl << B_BLACK << RED << i + 1 << BLUE << "|";
		for (int m = 0; m < 8; m++) {
			if (board[i][m].is_empty()) {
				tmpmove = "  ";
				tmpmove[0] = m + 'a';
				tmpmove[1] = i + '0' + 1;
				//cout << tmpmove;
				if(is_legal(tmpmove)){
					cout << B_BLACK << RED << "| " << BLUE << "*";
					cout << RED << " |";
				}else{
					cout << B_BLACK << RED << "|   |";
				}
			}
			else if (board[i][m].is_black()) {
				cout << B_BLACK << RED << "| " << MAGENTA << "B" << RED << " |";
			}
			else if (board[i][m].is_white()) {
				cout << B_BLACK << RED << "| " << WHITE << "W" << RED << " |";
			}
		}
		cout << B_BLACK << BLUE << "|";
		cout << endl;
	}
	cout << B_BLACK << BLUE << " |----------------------------------------|" << RESET << endl;

}
void Othello::restart() {
	skips = 0;
	movenum = 0;
	for (int i = 0; i < 8; i++) {
		for (int m = 0; m < 8; m++) {
			board[i][m].set_empty();
		}
	}
	board[3][3].set_white();
	board[3][4].set_black();
	board[4][3].set_black();
	board[4][4].set_white();
	if(!dummy.empty()){ //gives evaluate_moves something to compare to
		dummy.pop();
	}else{
		dummy.push("woop");
	}
}
bool Othello::is_legal(const std::string & move) const {
	int row, column;
	row = move[1] - '0' - 1;
	column = get_column(move[0]);
	if (!board[row][column].is_empty()){
		return false;
	}
	if (column == 10 || (row < 0 || row > 7)) {
		return false;
	}

	if (movenum % 2 == 0) { //black's turn
		if (board[row - 1][column].is_white()) {
			for (int i = row - 1; i >= 0; i--) {
				if(board[i][column].is_empty()){
					break;
				}
				if (board[i][column].is_black()) {
					return true;
				}
			}
		}
		if (board[row + 1][column].is_white()) {
			for (int i = row + 1; i <= 7; i++) {
				if(board[i][column].is_empty()){
					break;
				}
				if (board[i][column].is_black()) {
					return true;
				}
			}
		}
		if (board[row][column + 1].is_white()) {
			for (int i = column + 1; i <= 7; i++) {
				if(board[row][i].is_empty()){
					break;
				}
				if (board[row][i].is_black()) {
					return true;
				}
			}
		}
		if (board[row][column - 1].is_white()) {
			for (int i = column - 1; i >= 0; i--) {
				if(board[row][i].is_empty()){
					break;
				}
				if (board[row][i].is_black()) {
					return true;
				}
			}
		}
		//diagonals. check for accuracy.
		if (board[row + 1][column + 1].is_white()) {
			for (int i = column + 1, m = row + 1; i <= 7 && m <= 7; i++, m++) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_black()) {
					return true;
				}
			}
		}
		if (board[row - 1][column - 1].is_white()) {
			for (int i = column - 1, m = row - 1; i >= 0 && m >= 0; i--, m--) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_black()) {
					return true;
				}
			}
		}
		if (board[row + 1][column - 1].is_white()) {
			for (int i = column - 1, m = row + 1; i >= 0 && m <= 7; i--, m++) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_black()) {
					return true;
				}
			}
		}
		if (board[row - 1][column + 1].is_white()) {
			for (int i = column + 1, m = row - 1; i <= 7 && m >= 0; i++, m--) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_black()) {
					return true;
				}
			}
		}
	}
	else {//white's turn

		if (board[row - 1][column].is_black()) {
			for (int i = row - 1; i >= 0; i--) {
				if(board[i][column].is_empty()){
					break;
				}
				if (board[i][column].is_white()) {
					return true;
				}
			}
		}
		if (board[row + 1][column].is_black()) {
			for (int i = row + 1; i <= 7; i++) {
				if(board[i][column].is_empty()){
					break;
				}
				if (board[i][column].is_white()) {
					return true;
				}
			}
		}
		if (board[row][column + 1].is_black()) {
			for (int i = column + 1; i <= 7; i++) {
				if(board[row][i].is_empty()){
					break;
				}
				if (board[row][i].is_white()) {
					return true;
				}
			}
		}
		if (board[row][column - 1].is_black()) {
			for (int i = column - 1; i >= 0; i--) {
				if(board[row][i].is_empty()){
					break;
				}
				if (board[row][i].is_white()) {
					return true;
				}
			}
		}
		//diagonals.
		if (board[row + 1][column + 1].is_black()) {
			for (int i = column + 1, m = row + 1; i <= 7 && m <= 7; i++, m++) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_white()) {
					return true;
				}
			}
		}
		if (board[row - 1][column - 1].is_black()) {
			for (int i = column - 1, m = row - 1; i >= 0 && m >= 0; i--, m--) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_white()) {
					return true;
				}
			}
		}
		if (board[row + 1][column - 1].is_black()) {
			for (int i = column - 1, m = row + 1; i >= 0 && m <= 7; i--, m++) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_white()) {
					return true;
				}
			}
		}
		if (board[row - 1][column + 1].is_black()) {
			for (int i = column + 1, m = row - 1; i <= 7 && m >= 0; i++, m--) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_white()) {
					return true;
				}
			}
		}
	}
	return false;
}
void Othello::make_move(const std::string & move) {
	int row, column;
	if(move == "s"){
		if(!check_moves()){ //double check
			cout << "\nPlayer had no moves. Skipped turn.\n\n";
			skips++;
			movenum++;
			return;
		}
	}
	column = get_column(move[0]);
	row = move[1] - '0' - 1;
	if (movenum % 2 == 0) { //black's turn
		board[row][column].set_black();

		if (board[row - 1][column].is_white()) {
			for (int i = row - 1; i >= 0; i--) {
				if(board[i][column].is_empty()){
					break;
				}
				if (board[i][column].is_black()) {
					for (int m = i + 1; m < row; m++) { //flip everything within
						board[m][column].flip();
					}
					break;
				}
			}
		}
		if (board[row + 1][column].is_white()) {
			for (int i = row + 1; i <= 7; i++) {
				if(board[i][column].is_empty()){
					break;
				}
				if (board[i][column].is_black()) {
					for (int m = i - 1; m > row; m--) {
						board[m][column].flip();
					}
					break;
				}
			}
		}
		if (board[row][column + 1].is_white()) {
			for (int i = column + 1; i <= 7; i++) {
				if(board[row][i].is_empty()){
					break;
				}
				if (board[row][i].is_black()) {
					for (int m = i - 1; m > column; m--) {
						board[row][m].flip();
					}
					break;
				}
			}
		}
		if (board[row][column - 1].is_white()) {
			for (int i = column - 1; i >= 0; i--) {
				if(board[row][i].is_empty()){
					break;
				}
				if (board[row][i].is_black()) {
					for (int m = i + 1; m < column; m++) {
						board[row][m].flip();
					}
					break;
				}
			}
		}
		//diagonals. check for accuracy.
		if (board[row + 1][column + 1].is_white()) {
			for (int i = column + 1, m = row + 1; i <= 7 && m <= 7; i++, m++) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_black()) {
					for (int j = i - 1, k = m - 1; k > row && j > column; k--, j--) {
						board[k][j].flip();
					}
					break;
				}
			}
		}
		if (board[row - 1][column - 1].is_white()) {
			for (int i = column - 1, m = row - 1; i >= 0 && m >= 0; i--, m--) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_black()) {
					for (int j = i + 1, k = m + 1; k < row && j < column; k++, j++){
						board[k][j].flip();
					}
					break;
				}
			}
		}
		if (board[row + 1][column - 1].is_white()) {
			for (int i = column - 1, m = row + 1; i >= 0 && m <= 7; i--, m++) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_black()) {
					for (int j = i + 1, k = m - 1; k > row, j < column; k--, j++) {
						board[k][j].flip();
					}
					break;
				}
			}
		}
		if (board[row - 1][column + 1].is_white()) {
			for (int i = column + 1, m = row - 1; i <= 7 && m >= 0; i++, m--) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_black()) {
					for (int j = i - 1, k = m + 1; k < row, j > column; k++, j--) {
						board[k][j].flip();
					}
					break;
				}
			}
		}
	}
	else {
		board[row][column].set_white(); //white's turn

		if (board[row - 1][column].is_black()) {
			for (int i = row - 1; i >= 0; i--) {
				if(board[i][column].is_empty()){
					break;
				}
				if (board[i][column].is_white()) {
					for (int m = i + 1; m < row; m++) { //flip everything within
						board[m][column].flip();
					}
					break;
				}
			}
		}
		if (board[row + 1][column].is_black()) {
			for (int i = row + 1; i <= 7; i++) {
				if(board[i][column].is_empty()){
					break;
				}
				if (board[i][column].is_white()) {
					for (int m = i - 1; m > row; m--) {
						board[m][column].flip();
					}
					break;
				}
			}
		}
		if (board[row][column + 1].is_black()) {
			for (int i = column + 1; i <= 7; i++) {
				if(board[row][i].is_empty()){
					break;
				}
				if (board[row][i].is_white()) {
					for (int m = i - 1; m > column; m--) {
						board[row][m].flip();
					}
					break;
				}
			}
		}
		if (board[row][column - 1].is_black()) {
			for (int i = column - 1; i >= 0; i--) {
				if(board[row][i].is_empty()){
					break;
				}
				if (board[row][i].is_white()) {
					for (int m = i + 1; m < column; m++) {
						board[row][m].flip();
					}
					break;
				}
			}
		}
		//diagonals. check for accuracy.
		if (board[row + 1][column + 1].is_black()) {
			for (int i = column + 1, m = row + 1; i <= 7 && m <= 7; i++, m++) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_white()) {
					for (int j = i - 1, k = m - 1; k > row && j > column; k--, j--) {
						board[k][j].flip();
					}
					break;
				}
			}
		}
		if (board[row - 1][column - 1].is_black()) {
			for (int i = column - 1, m = row - 1; i >= 0 && m >= 0; i--, m--) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_white()) {
					for (int j = i + 1, k = m + 1; k < row && j < column; k++, j++) {
						board[k][j].flip();
					}
					break;
				}
			}
		}
		if (board[row + 1][column - 1].is_black()) {
			for (int i = column - 1, m = row + 1; i >= 0 && m <= 7; i--, m++) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_white()) {
					for (int j = i + 1, k = m - 1; k > row, j < column; k--, j++) {
						board[k][j].flip();
					}
					break;
				}
			}
		}
		if (board[row - 1][column + 1].is_black()) {
			for (int i = column + 1, m = row - 1; i <= 7 && m >= 0; i++, m--) {
				if(board[m][i].is_empty()){
					break;
				}
				if (board[m][i].is_white()) {
					for (int j = i - 1, k = m + 1; k < row, j > column; k++, j--) {
						board[k][j].flip();
					}
					break;
				}
			}
		}
	}
	skips = 0;
	movenum++;
}
int get_column(char move) {
        switch (move) {
        case 'a':
        case 'A':
                return 0;
        case 'b':
        case 'B':
                return 1;
        case 'c':
        case 'C':
                return 2;
        case 'd':
        case 'D':
                return 3;
        case 'e':
        case 'E':
                return 4;
        case 'f':
        case 'F':
                return 5;
        case 'g':
        case 'G':
                return 6;
        case 'h':
        case 'H':
                return 7;
        default:
                return 10; //means move is not legal
        }
}
bool Othello :: is_game_over( ) const{
	if(skips > 1){
		return true;
	}
	bool allwhite = true;
	bool allblack = true;
	for (int i = 0; i < 8; i++) {
		for (int m = 0; m < 8; m++) {
			if(board[i][m].is_empty()){
				return false;
			}else if(board[i][m].is_black()){
				allwhite = false;
			}else if(board[i][m].is_white()){
				allblack = false;
			}
		}
	}
	if(allwhite || allblack){
		return true;
	}
	return true;
}
int Othello :: countwhite() const{
	int sum = 0;
	for (int i = 0; i < 8; i++) {
		for (int m = 0; m < 8; m++) {
			if(board[i][m].is_white()){
				sum++;
			}
		}
	}
	return sum;
}
int Othello :: countblack() const{
	int sum = 0;
	for (int i = 0; i < 8; i++) {
		for (int m = 0; m < 8; m++) {
			if(board[i][m].is_black()){
				sum++;
			}
		}
	}
	return sum;
}
int Othello :: evaluate( ) const{
	//computer is white, so doing this for white pieces I guess
	int sum = 0;
	for (int i = 0; i < 8; i++) {
		for (int m = 0; m < 8; m++) {
			if(board[i][m].is_white()){
				sum++;
			}else if(board[i][m].is_black()){
				sum--;
			}
			if((i == 0 && m == 0) || (i == 0 && m == 7) || (i == 7 && m == 0) || (i == 7 && m == 7)){
				if(board[i][m].is_white()){
					sum += 10;
				}else if(board[i][m].is_black()){
					sum -= 10;
				}	
			}else if(i == 0 || i == 7 || m == 0 || m == 7){
				if(board[i][m].is_white()){
					sum += 3;
				}else if(board[i][m].is_black()){
					sum -= 3;
				}
			}
		}
	}
}
void Othello :: display_message(const std::string& message) const{
	int blackp = countblack();
	int whitep = countwhite();
	cout << "~o~o~o~o~o~o~o~o~o~o~o~o~o~o~o~o~o~o~" << endl;
	cout << "Turn: ";
	if(movenum % 2 == 0){
		cout << "BLACK  (move ";
	}else{
		cout << "WHITE  (move ";
	}
	cout << movenum + 1 << ")" << endl;
	cout << "B:  " << blackp << "       ";
	cout << "W:  " << whitep << endl;
	cout << "~o~o~o~o~o~o~o~o~o~o~o~o~o~o~o~o~o~o~" << endl;
	cout << message;
}
int Othello :: compute_moves(std::queue<std::string>& moves) const{
	string tmpmove;
	bool hasMoves = false;
	for(int i = 0; i < 8; i++){
		for(int m = 0; m < 8; m++){
			tmpmove = "  ";
			tmpmove[0] = m + 'a';
			tmpmove[1] = i + '0' + 1;
			if(is_legal(tmpmove)){
				moves.push(tmpmove);
			}
		}
	}
	return hasMoves;
}
bool Othello :: check_moves(){
	string tmpmove;
	bool hasMoves = false;
	for(int i = 0; i < 8; i++){
		for(int m = 0; m < 8; m++){
			tmpmove = "  ";
			tmpmove[0] = m + 'a';
			tmpmove[1] = i + '0' + 1;
			if(is_legal(tmpmove)){
				hasMoves = true;
			}
		}
	}
	return hasMoves;
}
